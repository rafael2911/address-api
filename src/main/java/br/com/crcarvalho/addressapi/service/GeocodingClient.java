package br.com.crcarvalho.addressapi.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.crcarvalho.addressapi.controller.dto.GeocodingDto;

@FeignClient(name = "geocoding", url = "${geocoding.url}")
public interface GeocodingClient {
	
	@GetMapping
	public GeocodingDto getGeocoding(@RequestParam String address, @RequestParam String key);
	
}
