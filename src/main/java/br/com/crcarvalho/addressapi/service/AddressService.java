package br.com.crcarvalho.addressapi.service;

import java.util.List;

import br.com.crcarvalho.addressapi.controller.dto.AddressDto;
import br.com.crcarvalho.addressapi.controller.form.AddressForm;
import br.com.crcarvalho.addressapi.entity.Address;

public interface AddressService {
	
	List<AddressDto> getAll();
	AddressDto getAddress(Long id);
	Address save(AddressForm form);
	void delete(Long id);
	AddressDto update(Long id, AddressForm form);

}
