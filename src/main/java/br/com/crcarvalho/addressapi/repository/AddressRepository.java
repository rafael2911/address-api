package br.com.crcarvalho.addressapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.crcarvalho.addressapi.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
